package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    boolean isSeparator(char c) {
        return c == '.';
    }

    boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    void processOperator(LinkedList<Double> st, char op) {
        double r = st.removeLast();
        double l = st.removeLast();
        switch (op) {
            case '+':
                st.add(l + r);
                break;
            case '-':
                st.add(l - r);
                break;
            case '*':
                st.add(l * r);
                break;
            case '/':
                if (r == 0) {
                    throw new ArithmeticException();
                } else
                    st.add(l / r);
                break;
        }
    }

    double roundResult(double value) {
        double shift = Math.pow(10, 4);
        return Math.round(value * shift) / shift;
    }

    String evaluate(String s) {
        try {
            LinkedList<Double> st = new LinkedList<>();
            LinkedList<Character> op = new LinkedList<>();
            s = s.replace(" ", "").replace("(-", "(0-").replace(",-", ",0-");
            if (s.charAt(0) == '-')
                s = "0" + s;
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (c == '(')
                    op.add('(');
                else if (c == ')') {
                    while (op.getLast() != '(')
                        processOperator(st, op.removeLast());
                    op.removeLast();
                } else if (isOperator(c)) {
                    while (!op.isEmpty() && priority(op.getLast()) >= priority(c))
                        processOperator(st, op.removeLast());
                    op.add(c);
                } else {
                    String operand = "";
                    while (i < s.length() && Character.isDigit(s.charAt(i))) {
                        operand += s.charAt(i++);
                    }
                    --i;
                    if (i != s.length() - 1 && isSeparator(s.charAt(i + 1)) && !isSeparator(s.charAt(i + 2)) && !isSeparator(s.charAt(0))) {
                        operand += ".";
                        i += 2;
                        while (i < s.length() && Character.isDigit(s.charAt(i)))
                            operand += s.charAt(i++);
                        i--;
                        st.add(Double.parseDouble(operand));
                    } else
                        st.add(Double.parseDouble(operand));
                }
            }
            while (!op.isEmpty())
                processOperator(st, op.removeLast());
            Double d = roundResult(st.get(0));
            return d.toString().replaceAll("\\.0+$", "");
        } catch (Exception e) {
            return null;
        }
    }
}
