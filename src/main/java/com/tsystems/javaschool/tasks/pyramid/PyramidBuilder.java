package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    boolean isCanBuild(List<Integer> inputNumbers) {
        int s = 0, i = 0;
        while (s < inputNumbers.size()) {
            i++;
            s += i;
        }
        if (s == inputNumbers.size()) return true;
        else return false;
    }

    int getRowCount(List<Integer> inputNumbers) {
        int s = 0, i = 0;
        while (s < inputNumbers.size()) {
            i++;
            s += i;
        }
        return i;
    }

    int getColumnCount(List<Integer> inputNumbers) {
        return getRowCount(inputNumbers) * 2 - 1;
    }

    public int[][] buildPyramid(List<Integer> input) {
        int[][] answ;
        try {
            if (isCanBuild(input) && !input.contains(null)) {
                answ = new int[getRowCount(input)][getColumnCount(input)];
                Collections.sort(input);
                for (int i = 0; i < answ.length; i++)
                    for (int j = 0; j < answ[i].length; j++) {
                        answ[i][j] = 0;
                    }
                int p = getColumnCount(input) / 2;
                int g = p;
                int count = 0;
                answ[0][p] = input.get(count);
                count++;
                for (int i = 1; i < answ.length; i++) {
                    p--;
                    g++;
                    for (int j = p; j <= g; j++) {
                        answ[i][j] = input.get(count);
                        count++;
                        j++;
                    }
                }
            } else throw new CannotBuildPyramidException();
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
        return answ;
    }
}
