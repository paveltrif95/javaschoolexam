package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {
    @SuppressWarnings("rawtypes")
    boolean find(List x, List y) {
        boolean b = false;
        if (x == null || y == null) throw new IllegalArgumentException();
        else if (x.equals(y)) b = true;
        else if (x.size() < y.size()) {
            List z = new ArrayList();
            int p = 0;
            for (int i = 0; i < x.size(); i++) {
                for (int j = p; j < y.size(); j++) {
                    if (x.get(i).equals(y.get(j))) {
                        z.add(x.get(i));break;
                    }
                    p = j + 1;
                }
            }
            if (x.equals(z)) b = true;
        }
        return b;
    }
}
